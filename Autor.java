public class Autor extends Persona {
    //Declaraciones de variables
    private int numLibros,maxPaginasEscritas,aniosExp;
    private String nombreArtistico;
    private int edad;

    //Constructores
    public Autor(String nombre){
        super(nombre);
    }

    //Secciones de set
    public void setNumLibros(int numLibros){
        this.numLibros=numLibros;
    }
    public void setMaxPaginas(int maxPaginasEscritas){
        this.maxPaginasEscritas=maxPaginasEscritas;
    }
    public void setAniosExp(int aniosExp){
        this.aniosExp=aniosExp;
    }
    public void setNombreArtistico(String nombreArtistico){
        this.nombreArtistico=nombreArtistico;
    }

    //Definiciones de get
    public int getNumLibros(){
        return numLibros;
    }
    public int getMaxPaginas(){
        return maxPaginasEscritas;
    }
    public int getAniosExp(){
        return aniosExp;
    }
    public String getNombreArtistico(){
        return nombreArtistico;
    }
    
    //Metodos diferentes a set y get
    public void imprime(){
        super.imprime();
        System.out.println("He escrito "+numLibros+" libros "+this.edad);
    }
}