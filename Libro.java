public class Libro {
    private String titulo,autor,editorial;
    private int numPag, anioImpresion, anioDistribucion, isbn;
    static int numero;

    //Constructores
    public Libro(){
        numero++;
    }
    public Libro(String titulo, String autor){
        this.titulo=titulo;
        this.autor=autor;
        numero++;
    }

    //Metodos setters
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    public void setAutor(String autor) {
        this.autor = autor;
    }
    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }
    public void setNumPag(int numPag) {
        this.numPag = numPag;
    }
    public void setAnioImpresion(int anioImpresion) {
        this.anioImpresion = anioImpresion;
    }
    public void setAnioDistribucion(int anioDistribucion) {
        this.anioDistribucion = anioDistribucion;
    }
    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    //Metodo getters
    public String getTitulo() {
        return titulo;
    }
    public String getAutor() {
        return autor;
    }
    public String getEditorial() {
        return editorial;
    }
    public int getNumPag() {
        return numPag;
    }
    public int getAnioImpresion() {
        return anioImpresion;
    }
    public int getAnioDistribucion() {
        return anioDistribucion;
    }
    public int getIsbn() {
        return isbn;
    }
}
