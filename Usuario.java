public class Usuario extends Persona{
    private String clave, calle, colonia, ciudad;
    private int id;
    
    public Usuario(String nombre){
        this.nombre=nombre;
    }

    //Metodos set
    public void setClave(String clave) {
        this.clave = clave;
    }
    public void setCalle(String calle) {
        this.calle = calle;
    }
    public void setColonia(String colonia) {
        this.colonia = colonia;
    }
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
    public void setId(int id) {
        this.id = id;
    }

    //Metodos get
    public String getClave() {
        return clave;
    }
    public String getCalle() {
        return calle;
    }
    public String getColonia() {
        return colonia;
    }
    public String getCiudad() {
        return ciudad;
    }
    public int getId() {
        return id;
    }


}
